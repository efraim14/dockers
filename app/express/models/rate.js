const config = require('../config'),
    dbConfig = config.dbConfig,
    mysql      = require('promise-mysql');

module.exports = {
    getRate7Days: async (dates) => {
        let connection = await mysql.createConnection(dbConfig);
        let query = `SELECT t1.KursId, t1.KursFrom, t1.KursTo, t2.Rate, IFNULL(t3.Average, null) AS Average7days FROM kurs t1 LEFT JOIN rate t2 ON t1.KursId = t2.KursId AND t2.Dates LIKE '${dates}' `;
        query += `LEFT JOIN (SELECT KursId, AVG(Rate) AS Average FROM rate WHERE Dates BETWEEN (DATE('${dates}') - INTERVAL 6 DAY) AND DATE('${dates}') GROUP BY KursId HAVING COUNT(RateId) = 7) t3 ON t1.KursId = t3.KursId`;
        let values = [];
        let data = await connection.query(query, values);
        connection.end();
        return data;
    },
    insertRate: async(data) => {
        let connection = await mysql.createConnection(dbConfig);
        let values = [data.KursId, data.Dates, data.Rate];
        let result = await connection.query("INSERT INTO rate(KursId, Dates, Rate) VALUES (?,?,?)", values);
        connection.end();
        return result;
    },
    getRate: async(data) => {
        let connection = await mysql.createConnection(dbConfig);
        let values = [data.Dates];
        let result = await connection.query("SELECT * FROM rate WHERE Dates LIKE ?", values);
        connection.end();
        return result;
    },
    deleteRateByKursId: async(id) => {
        let connection = await mysql.createConnection(dbConfig);
        let values = [id];
        let result = await connection.query("DELETE FROM rate WHERE KursId=?", values);
        connection.end();
        return result;
    }
}