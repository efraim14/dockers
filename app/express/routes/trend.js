var express = require('express');
var router = express.Router();
var rp = require('request-promise');
var config = require('../config');

/* GET users listing. */
router.get('/:id', async (req, res) => {
  let param = 'trend/' + req.params.id;
  let options = {
    url: config.restConfig.url + param,
    method: 'GET',
    timeout: config.restConfig.timeout
  };

  let data = await rp(options);
  console.log(data);
  res.render('trend/index', { data: JSON.parse(data) });
});

module.exports = router;
