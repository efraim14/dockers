const config = require('../config'),
    dbConfig = config.dbConfig,
    mysql      = require('promise-mysql');

module.exports = {
    getKurs: async () => {
        let connection = await mysql.createConnection(dbConfig);
        let data = await connection.query("SELECT * FROM kurs");
        connection.end();
        return data;
    },
    insertKurs: async(data) => {
        let connection = await mysql.createConnection(dbConfig);
        let values = [data.KursFrom, data.KursTo];
        let result = await connection.query("INSERT INTO kurs(KursFrom, KursTo) VALUES (?,?)", values);
        connection.end();
        return result;
    },
    deleteKurs: async(id) => {
        let connection = await mysql.createConnection(dbConfig);
        let values = [id];
        let result = await connection.query("DELETE FROM kurs WHERE KursId=?", values);
        connection.end();
        return result;
    }
}