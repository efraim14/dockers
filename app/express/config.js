exports.dbConfig = {
  host     : 'localhost',
  user     : 'users',
  password : '123',
  database : 'test'
};

exports.restConfig = {
    url: 'http://localhost:3000/api/',
    timeout: 30000
}