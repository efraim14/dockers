var express = require('express');
var router = express.Router();
var rate = require('../models/rate');
var kurs = require('../models/kurs');
var trend = require('../models/trend');

/* GET users listing. */
router.get('/rate/average/:date', async (req, res) => {
  let data = await rate.getRate7Days(req.params.date);
  res.send(data);
});

router.post('/rate/store', async (req, res) => {
  try {
    let validate = await rate.getRate(req.body);
    if(validate.length) {
      return res.send({error: 'Exchange Rate for that date is already exist'});
    }
    let data = await rate.insertRate(req.body);
    res.send(data);
  } catch (err) {
    return res.status(400).send({ error: err });
  }
});

router.post('/kurs/store', async (req, res) => {
  try {
    let data = await kurs.insertKurs(req.body);
    res.send(data);
  } catch (err) {
    return res.status(400).send({ error: err });
  }
});


router.get('/kurs', async (req, res) => {
  let data = await kurs.getKurs();
  res.send(data);
});

router.get('/kurs/delete/:id', async (req, res) => {
  let data = await kurs.deleteKurs(req.params.id);
  await rate.deleteRateByKursId(req.params.id);
  res.send(data);
});

router.get('/trend/:id', async (req, res) => {
  let data = await trend.getTrend(req.params.id);
  res.send(data);
});
module.exports = router;
