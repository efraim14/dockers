var express = require('express');
var router = express.Router();
var rp = require('request-promise');
var config = require('../config');

/* GET users listing. */
router.get('/', async (req, res) => {
  let param = 'kurs';
  let options = {
    url: config.restConfig.url + param,
    method: 'GET',
    timeout: config.restConfig.timeout
  };

  let data = await rp(options);
  res.render('kurs/index', { data: JSON.parse(data) });
});

router.get('/create', async (req, res) => {
  res.render('kurs/create');
});

router.get('/delete/:id', async (req, res) => {
  let param = 'kurs/delete/' + req.params.id;
  let options = {
    url: config.restConfig.url + param,
    method: 'GET',
    timeout: config.restConfig.timeout,
  };

  let data = await rp(options);

  res.redirect('/exchange');
});

router.post('/store', async (req, res) => {
  let param = 'kurs/store';
  let options = {
    url: config.restConfig.url + param,
    method: 'POST',
    timeout: config.restConfig.timeout,
    body: req.body,
    json: true
  };

  let data = await rp(options);

  res.redirect('/exchange');
});

module.exports = router;
