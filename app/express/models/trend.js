const config = require('../config'),
    dbConfig = config.dbConfig,
    mysql = require('promise-mysql');

module.exports = {
    getTrend: async (id) => {
        let connection = await mysql.createConnection(dbConfig);
        let values = [id];
        let data = await connection.query("SELECT t1.KursFrom, t1.KursTo, DATE_FORMAT(t2.Dates, '%d/%m/%Y') AS Dates, t2.Rate FROM kurs t1 JOIN rate t2 ON t1.KursId = t2.KursId WHERE t1.KursId = ? AND t2.Dates BETWEEN (DATE(NOW()) - INTERVAL 6 DAY) AND DATE(NOW()) ORDER BY t2.Dates DESC", values);
        connection.end();
        return data;
    }
}