var express = require('express');
var router = express.Router();
var rp = require('request-promise');
var config = require('../config');

/* GET users listing. */
router.get('/', async (req, res) => {
 
  res.render('rate/index', {data: null, dates: null} );
});

router.get('/average/:date', async (req, res) => {
  let param = 'rate/average/' + req.params.date;
  let options = {
    url: config.restConfig.url + param,
    method: 'GET',
    timeout: config.restConfig.timeout
  };

  let data = await rp(options);
  
  res.render('rate/index', { data: JSON.parse(data), dates: req.params.date });
});

router.get('/create/:message?', async (req, res) => {
  try {
        let param = 'kurs';
        let options = {
            url: config.restConfig.url + param,
            method: 'GET',
            timeout: config.restConfig.timeout
        };

        let data = await rp(options);
        res.render('rate/create', {data: JSON.parse(data), message: req.params.message});
    } catch (err) {
        return res.status(500).send({error: err});
    }
});

router.post('/store', async (req, res) => {
  let param = 'rate/store';
  let options = {
    url: config.restConfig.url + param,
    method: 'POST',
    timeout: config.restConfig.timeout,
    body: req.body,
    json: true
  };

  let data = await rp(options);
  if (data.error) {
    return res.redirect('/rates/create/'+data.error);
  }

  res.redirect('/rates');
});

module.exports = router;
